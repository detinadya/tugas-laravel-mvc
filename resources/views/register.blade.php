<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<h1>Buat Account Baru</h1>
<h3>Sign Up Form</h3>

      <form action="/welcome" method="post">
        @csrf
    
      <label>First name:</label> <br><br>
      <input type="text" name="first"> <br><br>
      <label>Last name:</label> <br><br>
      <input type="text" name="last"> <br><br>
      <label>Gender:</label> <br><br>

      <!-- ditambahkan atribut 'name' biar cuma bs pilih salah 1 -->
      <input type="radio" name="gender">Male <br>
      <input type="radio" name="gender">Female <br>
      <input type="radio" name="gender">Other <br><br>

      <label>Nationality:</label> <br><br>
      <select name="nationality"> <br>
        <option value="1">Indonesian</option> <br><br>
        <option value="2">Malaysian</option> <br><br>
      </select>

      <br><br>
      <label>Language Spoken:</label> <br><br>
      <input type="checkbox">Bahasa Indonesia <br>
      <input type="checkbox">English <br>
      <input type="checkbox">Other <br><br>

      <label>Bio:</label> <br><br>
      <textarea name="bio" rows="10" cols="30"></textarea> <br><br>

      <input type="submit" name="submit" value="submit">
      <input type="reset" name="clear" value="clear">


    </form>

</body>
</html>