@extends('adminlte.master')

@section('content')

    <div class="ml-3 mt-3">
        <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Pertanyaan {{ $pertanyaan->id }}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/pertanyaan/{{ $pertanyaan->id }}" method="POST">
                  @csrf <!-- krn pake method post -->
                  @method('put')
                <div class="card-body">
                  <div class="form-group">
                    <label for="judul">Judul</label>
                    <input type="text" class="form-control" id="judul" value=" {{ old('judul', $pertanyaan->judul) }} " name ="judul" placeholder="Masukkan Judul">
                    
                    @error('judul')
                     <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                  </div>
                  <div class="form-group">
                    <label for="isi">Pertanyaan</label>
                    <input type="text" class="form-control" id="isi" value=" {{ old('isi', $pertanyaan->isi) }} " name="isi" placeholder="Isi Pertanyaan">

                    @error('isi')
                     <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
              </form>
    </div>
    </div>
    
    

@endsection