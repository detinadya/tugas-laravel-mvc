@extends('adminlte.master')

@section('content')

    <div class="ml-3 mt-3">
        <div class="card">
              <div class="card-header">
                <h3 class="card-title">Pertanyaan</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                  @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                  @endif
                  <a href="/pertanyaan/create" class="btn btn-primary mb-2">Buat Pertanyaan</a> <!--btn primary utk warna biru -->
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">No</th>
                      <th>Judul Pertanyaan</th>
                      <th>Isi Pertanyaan</th>
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse ($pertanyaan as $key => $pertanyaan)
                        <tr>
                            <td> {{ $key + 1 }} </td>
                            <td> {{ $pertanyaan->judul }} </td>
                            <td> {{ $pertanyaan->isi }} </td>
                            <td style="display : flex;">
                                <a href="/pertanyaan/{{ $pertanyaan->id }}" class="btn btn-info btn-sm">show</a>
                                <a href="/pertanyaan/{{ $pertanyaan->id }}/edit" class="btn btn-default btn-sm">edit</a>
                                <form action="/pertanyaan/{{ $pertanyaan->id }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" class="btn btn-danger btn-sm" value="delete">
                                </form>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="4" align="center">Tidak Ada Pertanyaan</td>
                        </tr>
        
                    @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              
            </div>
    </div>

@endsection