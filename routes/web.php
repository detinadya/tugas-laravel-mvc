<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*  
 Route::get('/home', function () {
    return view('home');
});

Route::get('/register', 'AuthController@form');
Route::get('/sapa', 'AuthController@sapa');

Route::get('/welcome', 'AuthController@end');
Route::post('/welcome', 'AuthController@submit');


Route::get('/master', function () {
    return view('adminlte.master');
});

Route::get('/', function(){
    return view('index');
});
Route::get('/data-tables', function(){
    return view('data-tables'); 
}); */



Route::get('/pertanyaan/create', 'PertanyaanController@create');
Route::post('/pertanyaan', 'PertanyaanController@store');
Route::get('/pertanyaan', 'PertanyaanController@index');
Route::get('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@show');
Route::get('/pertanyaan/{pertanyaan_id}/edit', 'PertanyaanController@edit');
Route::put('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@update');
Route::delete('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@destroy');