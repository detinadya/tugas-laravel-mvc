<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB; //utk query builder -> ngambil data dari DB atau get data

class PertanyaanController extends Controller
{
    //

    public function create(){
        return view('pertanyaan.create');
    }

    public function store(Request $request){
        // dd($request->all()); utk ngeliat inputan datanya seperti apa
        //validasi data biar kl data kosong ada notif utk ux-nya
        $request->validate([
            'judul' => 'required|unique:pertanyaans|max:255',
            'isi' => 'required | max:255'
        ]);
        $query = DB::table('pertanyaans')->insert([
            'judul' => $request["judul"],
            'isi' => $request["isi"]
            ]
        );
        return redirect('/pertanyaan')->with('success' , 'Pertanyaan berhasil disimpan!');
    }

    public function index(){
        $pertanyaan = DB::table('pertanyaans')->get(); //select * from pertanyaans
        //dd('$pertanyaan');
        return view('pertanyaan.index' , compact('pertanyaan'));
    }

    public function show($id){
        $pertanyaan = DB::table('pertanyaans')->where('id', $id)->first();
       // dd($pertanyaan); 
        return view('pertanyaan.show' , compact('pertanyaan'));
    }

    public function edit($id){
        $pertanyaan = DB::table('pertanyaans')->where('id', $id)->first();
        return view('pertanyaan.edit' , compact('pertanyaan'));
    }

    public function update($id , Request $request){
        $query = DB::table('pertanyaans')
                        ->where('id', $id)
                        ->update([
                            'judul' => $request['judul'],
                            'isi' => $request['isi']

                        ]);
        return redirect('/pertanyaan')->with('success' , 'Berhasil update pertanyaan');
    }

    public function destroy($id){
        $query = DB::table('pertanyaans')->where('id' , $id)->delete(); 
        return redirect('/pertanyaan')->with('success' , 'Berhasil dihapus!');
    }
}
